import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  constructor() { }

  MensajePadre! : string;
  segundo! : string;
  tercer!: string;
  cuarto!: string;
  quinto!: string;


  Primer ($event: string): void {
    this.MensajePadre = $event;
    console.log(this.MensajePadre);  
  }

  Segundo ($event: string): void {
    this.segundo = $event;
  }

  Tercer ($event: string): void {
    this.tercer = $event;
  }

  Cuarto($event: string): void {
    this.cuarto = $event;
  }

  Quinto ($event: string): void {
    this.quinto = $event;
  }

  ngOnInit(): void {
  }

}
